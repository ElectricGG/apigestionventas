﻿using ApiVentas.Presenters.UpdateVenta;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesDTOs.UpdateVenta;
using ApiVentas.UsesCasesPorts.UpdateVenta;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.UpdateVenta
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UpdateVentaController
    {
        readonly IUpdateVentaInputPort _updateVentaInputPort;
        readonly IUpdateVentaOutputPort _updateVentaOutputPort;

        public UpdateVentaController(IUpdateVentaInputPort updateVentaInputPort,
                                    IUpdateVentaOutputPort updateVentaOutputPort)
        => (_updateVentaInputPort, _updateVentaOutputPort) = (updateVentaInputPort, updateVentaOutputPort);
        
        [HttpPut("update-venta")]
        public async Task<string> UpdateVenta(UpdateVentaParams updateVentaParams)
        {
            await _updateVentaInputPort.Handle(updateVentaParams);
            var OutputPort = _updateVentaOutputPort as UpdateVentaPresenter;
            return OutputPort.Content;
        }
    }
}

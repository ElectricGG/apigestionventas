﻿using ApiVentas.Presenters.VentaPresenter;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesDTOs.CreateVenta;
using ApiVentas.UsesCasesPorts.CreateVenta;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.CreateVenta
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CreateVentaController
    {
        readonly ICreateVentaInputPort _createVentaInputPort;
        readonly ICreateVentaOutPutPort _createVentaOutPutPort;
        public CreateVentaController(ICreateVentaInputPort createVentaInputPort, ICreateVentaOutPutPort createVentaOutPutPort)
            => (_createVentaInputPort,_createVentaOutPutPort) = (createVentaInputPort, createVentaOutPutPort);

        [HttpPost("create-venta")]
        public async Task<string> CreateVenta(CreateVentaParams createVentaParams)
        {
            await _createVentaInputPort.Handle(createVentaParams);
            var OutPutPort = _createVentaOutPutPort as CreateVentaPresenter;
            return OutPutPort.Content;
        }
    }
}

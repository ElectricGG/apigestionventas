﻿using ApiVentas.Presenters.GetProductos;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesDTOs.GetProductos;
using ApiVentas.UsesCasesPorts.GetProductos;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.GetProductosController
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GetProductosController
    {
        readonly IGetProductosInputPort _inputPort;
        readonly IGetProductosOutputPort _outputPort;
        public GetProductosController(IGetProductosInputPort inputPort, IGetProductosOutputPort outputPort)
        => (_inputPort, _outputPort) = (inputPort, outputPort);
        
        [HttpGet("lista")]
        public async Task<IEnumerable<GetProductosDTO>> GetProductos()
        {
            await _inputPort.Handle();
            var OutputPort = _outputPort as GetProductosPresenter;
            return OutputPort.Content;
        }
    }
}

﻿using ApiVentas.Presenters.GetAsesores;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesDTOs.GetAsesores;
using ApiVentas.UsesCasesPorts.GetAsesores;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.GetAsesoresController
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GetAsesoresController
    {
        readonly IGetAsesoresInputPort _inputPort;
        readonly IGetAsesoresOutputPort _outputPort;
        public GetAsesoresController(IGetAsesoresInputPort inputPort, IGetAsesoresOutputPort outputPort)
            => (_inputPort, _outputPort) = (inputPort,outputPort);
        
        [HttpGet("lista")]
        public async Task<IEnumerable<GetAsesoresDTO>> GetAsesores()
        {
            await _inputPort.Handle();
            var OutputPort = _outputPort as GetAsesoresPresenter;
            return OutputPort.Content;
        }
    }
}

﻿using ApiVentas.Presenters.DeleteVentas;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesPorts.DeleteVenta;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.DeleteVenta
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class DeleteVentaController
    {
        readonly IDeleteVentaInputPort _deleteVentaInputPort;
        readonly IDeleteVentaOutPutPort _deleteVentaOutPutPort;
        public DeleteVentaController(IDeleteVentaInputPort deleteVentaInputPort, IDeleteVentaOutPutPort deleteVentaOutPutPort)
            => (_deleteVentaInputPort, _deleteVentaOutPutPort) = (deleteVentaInputPort,deleteVentaOutPutPort);

        [HttpDelete("delete-venta")]
        public async Task<string> DeleteVenta(int id)
        {
            await _deleteVentaInputPort.Handle(id);
            var outputport = _deleteVentaOutPutPort as DeleteVentaPresenter;
            return outputport.Content;
        }
    }
}

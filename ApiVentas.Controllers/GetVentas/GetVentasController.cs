﻿using ApiVentas.Presenters.GetVentas;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesDTOs.GetVentas;
using ApiVentas.UsesCasesPorts.GetVentas;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.GetVentas
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GetVentasController
    {
        readonly IGetVentasInputPort _getVentasInputPort;
        readonly IGetVentasOutPutPort _getVentasOutPutPort;
        public GetVentasController(IGetVentasInputPort getVentasInputPort, IGetVentasOutPutPort getVentasOutPutPort)
            => (_getVentasInputPort, _getVentasOutPutPort) = (getVentasInputPort, getVentasOutPutPort);
        [HttpGet("lista")]
        public async Task<IEnumerable<GetVentasDTO>> GetOrders()
        {
            await _getVentasInputPort.Handle();
            var OutputPort = _getVentasOutPutPort as GetVentasPresenter;
            return OutputPort.Content;
        }
    }
}

﻿using ApiVentas.Presenters.GetClientes;
using ApiVentas.Security.Infrastructure.Helpers;
using ApiVentas.UsesCasesDTOs.GetClientes;
using ApiVentas.UsesCasesPorts.GetClientes;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Controllers.GetClientesController
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GetClientesController
    {
        readonly IGetClientesInputPort _inputPort;
        readonly IGetClientesOutputPort _outputPort;
        public GetClientesController(IGetClientesInputPort inputPort, IGetClientesOutputPort outputPort)
            => (_inputPort,_outputPort) = (inputPort,outputPort);
        
        [HttpGet("lista")]
        public async Task<IEnumerable<GetClientesDTO>> GetClientes()
        {
            await _inputPort.Handle();
            var OutputPort = _outputPort as GetClientesPresenter;
            return OutputPort.Content;
        }
    }
}

﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Repositories.EFCore.DataContext;
using ApiVentas.Repositories.EFCore.Repositories;
using ApiVentas.Security.Entities.Interfaces;
using ApiVentas.Security.Infrastructure.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Infrastructure
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddSecurity(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApiVentasContext>(options =>
                options.UseInMemoryDatabase("DBVentas")
            );
            services.AddScoped<IAsesorService, AsesorService>();
            services.AddScoped<IAsesorRepository, AsesorRepositry>();
            return services;
        }
    }
}

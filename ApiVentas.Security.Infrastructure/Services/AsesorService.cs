﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.Security.Entities.DTOs;
using ApiVentas.Security.Entities.Interfaces;
using ApiVentas.Security.Infrastructure.Helpers;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Infrastructure.Services
{
    public class AsesorService : IAsesorService
    {
        readonly IAsesorRepository _asesorRepository;

        private readonly SecuritySettings _securitySettings;
        private const double EXPIRY_DURATION_MINUTES = 30;

        public AsesorService(IOptions<SecuritySettings> securitySettings, IAsesorRepository asesorRepository)
            => (_securitySettings, _asesorRepository) = (securitySettings.Value, asesorRepository);
        

        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var asesor = _asesorRepository.GetAll().SingleOrDefault(x => x.Usuario == model.Usuario && x.Contraseña == model.Contraseña);

            // return null if user not found
            if (asesor == null) return null;

            // authentication successful so generate jwt token
            var token = BuildToken(asesor);

            return new AuthenticateResponse(asesor, token);
        }

        public IEnumerable<Asesor> GetAll()
        {
            return _asesorRepository.GetAll();
        }

        public Asesor GetById(int id)
        {
            return _asesorRepository.GetAll().FirstOrDefault(x => x.Id == id);
        }

        // helper methods

        private string BuildToken(Asesor asesor)
        {
            var claims = new[] {
                new Claim(ClaimTypes.Name, asesor.Usuario),
                new Claim("id", asesor.Id.ToString()),
                //new Claim(ClaimTypes.Role, user.Role),
                new Claim(ClaimTypes.NameIdentifier,Guid.NewGuid().ToString())
            };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_securitySettings.Secret));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);
            var tokenDescriptor = new JwtSecurityToken(
                issuer: _securitySettings.Issuer,
                audience: _securitySettings.Audience,
                claims: claims,
                expires: DateTime.Now.AddMinutes(EXPIRY_DURATION_MINUTES),
                signingCredentials: credentials
            );

            var token = new JwtSecurityTokenHandler().WriteToken(tokenDescriptor);

            return token;

            //// generate token that is valid for 7 days
            //var tokenHandler = new JwtSecurityTokenHandler();
            //var key = Encoding.ASCII.GetBytes(_securitySettings.Secret);
            //var tokenDescriptor = new SecurityTokenDescriptor
            //{
            //    Subject = new ClaimsIdentity(new[] { new Claim("id", user.Id.ToString()) }),
            //    Expires = DateTime.UtcNow.AddDays(7),
            //    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            //};
            //var token = tokenHandler.CreateToken(tokenDescriptor);
            //return tokenHandler.WriteToken(token);
        }
    }
}

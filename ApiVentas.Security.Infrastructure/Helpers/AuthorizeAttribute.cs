﻿using ApiVentas.Entities.POCOEntities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Infrastructure.Helpers
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class AuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var user = (Asesor)context.HttpContext.Items["Asesor"];
            if (user == null)
            {
                // not logged in
                context.Result = new JsonResult(new { message = "Sin autorizacion" }) { StatusCode = StatusCodes.Status401Unauthorized };
            }
        }
    }
}

﻿using ApiVentas.Security.Entities.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Infrastructure.Helpers
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly SecuritySettings _appSettings;

        public JwtMiddleware(RequestDelegate next, IOptions<SecuritySettings> appSettings)
            => (_next, _appSettings) = (next, appSettings.Value);

        public async Task Invoke(HttpContext context, IAsesorService asesorService)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
            {
                AttachUserToContext(context, asesorService, token);
            }

            await _next(context);
        }

        public JwtSecurityToken ValidatedToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidIssuer = _appSettings.Issuer,
                    ValidAudience = _appSettings.Audience,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;

                return jwtToken;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void AttachUserToContext(HttpContext context, IAsesorService asesorService, string token)
        {
            try
            {
                var jwtToken = ValidatedToken(token);
                var Id = int.Parse(jwtToken.Claims.First(x => x.Type == "id").Value);
                var asesor = asesorService.GetById(Id);
                context.Items["Asesor"] = asesor;
            }
            catch
            {
                
            }
        }
    }
}

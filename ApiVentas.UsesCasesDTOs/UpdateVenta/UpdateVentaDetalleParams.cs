﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.UpdateVenta
{
    public class UpdateVentaDetalleParams
    {
        public int ProductoId { get; set; }
        public decimal PrecioUnitario { get; set; }
        public short Cantidad { get; set; }
    }
}

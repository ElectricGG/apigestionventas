﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.UpdateVenta
{
    public class UpdateVentaParams
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int AsesorId { get; set; }
        public DateTime FechaVenta { get; set; }
        public List<UpdateVentaDetalleParams> VentaDetalles { get; set; }
    }
}

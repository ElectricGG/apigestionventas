﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.CreateVenta
{
    public class CreateVentaDetalleParams
    {
        public int ProductoId { get; set; }
        public decimal PrecioUnitario { get; set; }
        public short Cantidad { get; set; }
    }
}

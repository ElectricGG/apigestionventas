﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.CreateVenta
{
    public class CreateVentaParams
    {
        public int ClienteId { get; set; }
        public int AsesorId { get; set; }
        public DateTime FechaVenta { get; set; }
        public List<CreateVentaDetalleParams> VentaDetalles { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.GetAsesores
{
    public class GetAsesoresDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}

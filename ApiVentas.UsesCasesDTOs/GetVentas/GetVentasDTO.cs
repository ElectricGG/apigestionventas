﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.GetVentas
{
    public class GetVentasDTO
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public ClienteGetVentaDTO cliente { get; set; }
        public int AsesorId { get; set; }
        public AsesorGetVentaDTO asesor { get; set; }
        public DateTime FechaVenta { get; set; }
        public IEnumerable<GetVentasDetalleDTO> ventaDetalles { get; set; }
        public decimal MontoVenta { get; set; }
    }
}

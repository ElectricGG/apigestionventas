﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.GetVentas
{
    public class GetVentasDetalleDTO
    {
        public int VentaId { get; set; }
        public int ProductoId { get; set; }
        public ProductoGetVentasDetalleDTO producto { get; set; }
        public decimal PrecioUnitario { get; set; }
        public short Cantidad { get; set; }
        public decimal Subtotal { get; set; }
    }
}

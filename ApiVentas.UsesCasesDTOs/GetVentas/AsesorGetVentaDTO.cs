﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesDTOs.GetVentas
{
    public class AsesorGetVentaDTO
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}

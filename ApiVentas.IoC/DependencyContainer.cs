﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Presenters.DeleteVentas;
using ApiVentas.Presenters.GetAsesores;
using ApiVentas.Presenters.GetClientes;
using ApiVentas.Presenters.GetProductos;
using ApiVentas.Presenters.GetVentas;
using ApiVentas.Presenters.UpdateVenta;
using ApiVentas.Presenters.VentaPresenter;
using ApiVentas.Repositories.EFCore.DataContext;
using ApiVentas.Repositories.EFCore.Repositories;
using ApiVentas.Security.Entities.Interfaces;
using ApiVentas.Security.Infrastructure.Services;
using ApiVentas.UsesCases.CreateVenta;
using ApiVentas.UsesCases.DeleteVenta;
using ApiVentas.UsesCases.GetAsesores;
using ApiVentas.UsesCases.GetClientes;
using ApiVentas.UsesCases.GetProductos;
using ApiVentas.UsesCases.GetVentas;
using ApiVentas.UsesCases.UpdateVenta;
using ApiVentas.UsesCasesPorts.CreateVenta;
using ApiVentas.UsesCasesPorts.DeleteVenta;
using ApiVentas.UsesCasesPorts.GetAsesores;
using ApiVentas.UsesCasesPorts.GetClientes;
using ApiVentas.UsesCasesPorts.GetProductos;
using ApiVentas.UsesCasesPorts.GetVentas;
using ApiVentas.UsesCasesPorts.UpdateVenta;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.IoC
{
    public static class DependencyContainer
    {
        public static IServiceCollection AddApiVentasServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApiVentasContext>(options =>
                options.UseInMemoryDatabase("DBVentas")
            );
            #region Registro de repositorios
            services.AddScoped<IVentaRepository, VentaRepository>();
            services.AddScoped<IVentaDetalleRepository, VentaDetalleRepository>();
            services.AddScoped<IAsesorRepository, AsesorRepositry>();
            services.AddScoped<IClienteRepository, ClienteRepository>();
            services.AddScoped<IProductoRepository, ProductoRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            #endregion

            #region Registro de Casos de uso
            services.AddScoped<ICreateVentaInputPort, CreateVentaInteractor>();
            services.AddScoped<ICreateVentaOutPutPort, CreateVentaPresenter>();

            services.AddScoped<IUpdateVentaInputPort, UpdateVentaInteractor>();
            services.AddScoped<IUpdateVentaOutputPort, UpdateVentaPresenter>();

            services.AddScoped<IGetVentasInputPort, GetVentasInteractor>();
            services.AddScoped<IGetVentasOutPutPort, GetVentasPresenter>();

            services.AddScoped<IDeleteVentaInputPort, DeleteVentaInteractor>();
            services.AddScoped<IDeleteVentaOutPutPort, DeleteVentaPresenter>();

            services.AddScoped<IGetAsesoresInputPort, GetAsesoresInteractor>();
            services.AddScoped<IGetAsesoresOutputPort, GetAsesoresPresenter>();

            services.AddScoped<IGetClientesInputPort, GetClientesInteractor>();
            services.AddScoped<IGetClientesOutputPort, GetClientesPresenter>();

            services.AddScoped<IGetProductosInputPort, GetProductosInteractor>();
            services.AddScoped<IGetProductosOutputPort, GetProductosPresenter>();
            #endregion

            services.AddScoped<IAsesorService, AsesorService>();

            return services;
        }
    }
}

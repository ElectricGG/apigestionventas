﻿using ApiVentas.Entities.POCOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Entities.DTOs
{
    public class AuthenticateResponse
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Usuario { get; set; }
        public string Token { get; set; }


        public AuthenticateResponse(Asesor asesor, string token)
        {
            Id = asesor.Id;
            Nombre = asesor.Nombre;
            Usuario = asesor.Usuario;
            Token = token;
        }
    }
}

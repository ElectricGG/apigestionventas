﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Entities.DTOs
{
    public class AuthenticateRequest
    {
        [Required]
        public string Usuario { get; set; }

        [Required]
        public string Contraseña { get; set; }
    }
}

﻿using ApiVentas.Entities.POCOEntities;
using ApiVentas.Security.Entities.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Security.Entities.Interfaces
{
    public interface IAsesorService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        IEnumerable<Asesor> GetAll();
        Asesor GetById(int id);
    }
}

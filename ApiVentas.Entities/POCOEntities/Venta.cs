﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Entities.POCOEntities
{
    public class Venta
    {
        public int Id { get; set; }
        public int ClienteId { get; set; }
        public int AsesorId { get; set; }
        public DateTime FechaVenta { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Entities.POCOEntities
{
    public class VentaDetalle
    {
        public int VentaId { get; set; }
        public int ProductoId { get; set; }
        public decimal PrecioUnitario { get; set; }
        public short Cantidad { get; set; }
        public Venta Venta { get; set; }

    }
}

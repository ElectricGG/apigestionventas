﻿using ApiVentas.Entities.POCOEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Entities.Interfaces
{
    public interface IAsesorRepository
    {
        IEnumerable<Asesor> GetAll();
    }
}

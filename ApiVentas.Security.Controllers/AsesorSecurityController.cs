﻿using ApiVentas.Security.Entities.DTOs;
using ApiVentas.Security.Entities.Interfaces;
using ApiVentas.Security.Infrastructure.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;

namespace ApiVentas.Security.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AsesorSecurityController : ControllerBase
    {
        private readonly IAsesorService _asesorService;

        public AsesorSecurityController(IAsesorService userService) => _asesorService = userService;

        [HttpPost("authenticate")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _asesorService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Usuario o contraseña incorrecto" });

            return Ok(response);
        }

        [Authorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _asesorService.GetAll();
            return Ok(users);
        }
    }
}

﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.Repositories.EFCore.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Repositories.EFCore.Repositories
{
    public class AsesorRepositry : IAsesorRepository
    {
        readonly ApiVentasContext _context;
        public AsesorRepositry(ApiVentasContext context)
        {
            _context = context;
        }
        public IEnumerable<Asesor> GetAll()
        {
            if (_context.Asesores.Count() <= 0)
            {
                var Asesor = new List<Asesor> {
                    new Asesor { Id = 1, Nombre = "Jesus Cuadros", Usuario = "jcuadros", Contraseña = "test"},
                    new Asesor { Id = 2, Nombre = "Dan Peña", Usuario = "dam", Contraseña = "test"}
                };
                _context.Asesores.AddRange(Asesor);
                _context.SaveChanges();
            }
            
            return _context.Asesores;
        }

    }
}

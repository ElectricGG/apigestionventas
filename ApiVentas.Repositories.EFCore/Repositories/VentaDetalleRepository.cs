﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.Repositories.EFCore.DataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Repositories.EFCore.Repositories
{
    public class VentaDetalleRepository : IVentaDetalleRepository
    {
        readonly ApiVentasContext _context;
        public VentaDetalleRepository(ApiVentasContext context)
        {
            _context = context;
        }
        public void Create(VentaDetalle ventaDetalle)
        {
            _context.Add(ventaDetalle);
        }

        public IEnumerable<VentaDetalle> GetAll()
        {
            return _context.VentaDetalles;
        }

        public void Update(VentaDetalle ventaDetalle)
        {
            _context.VentaDetalles.Attach(ventaDetalle);
            _context.Entry(ventaDetalle).State = EntityState.Modified;
        }
    }
}

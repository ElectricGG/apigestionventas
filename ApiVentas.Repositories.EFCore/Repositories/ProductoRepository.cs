﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.Repositories.EFCore.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Repositories.EFCore.Repositories
{
    public class ProductoRepository : IProductoRepository
    {
        readonly ApiVentasContext _context;
        public ProductoRepository(ApiVentasContext context)
        {
            _context = context;
        }
        public IEnumerable<Producto> GetAll()
        {
            if (_context.Productos.Count() <= 0)
            {
                var producto = new List<Producto> {
                    new Producto { Id = 1, Nombre = "Lamborguini Huracan" },
                    new Producto { Id = 2, Nombre = "Lamborguini Aventador" },
                    new Producto { Id = 3, Nombre = "Lamborguini Urus" }
                };
                _context.Productos.AddRange(producto);
                _context.SaveChanges();
            }
            
            return _context.Productos;
        }
    }
}

﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.Repositories.EFCore.DataContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Repositories.EFCore.Repositories
{
    public class ClienteRepository : IClienteRepository
    {
        readonly ApiVentasContext _context;
        public ClienteRepository(ApiVentasContext context)
        {
            _context = context;
        }
        public IEnumerable<Cliente> GetAll()
        {
            if (_context.Clientes.Count() <= 0)
            {
                var cliente = new List<Cliente> {
                    new Cliente { Id = 1, Nombre = "Daniela Pastor" },
                    new Cliente { Id = 2, Nombre = "Adriana C M" },
                };
                _context.Clientes.AddRange(cliente);
                _context.SaveChanges();
            }
            
            return _context.Clientes;
        }
    }
}

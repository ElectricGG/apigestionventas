﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.Repositories.EFCore.DataContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Repositories.EFCore.Repositories
{
    public class VentaRepository : IVentaRepository
    {
        readonly ApiVentasContext _context;
        public VentaRepository(ApiVentasContext context)
        {
            _context = context;
        }
        public void Create(Venta venta)
        {
            _context.Add(venta);
        }
        public IEnumerable<Venta> GetAll()
        {
            return _context.Ventas;
        }
        public Venta GetVentaById(int id)
        {
            return _context.Ventas.Where(v => v.Id==id).FirstOrDefault();
        }
        public void Delete(Venta venta)
        {
            _context.Ventas.Remove(venta);
        }

        public void Update(Venta venta)
        {
            _context.Ventas.Attach(venta);
            _context.Entry(venta).State = EntityState.Modified;
        }
    }
}

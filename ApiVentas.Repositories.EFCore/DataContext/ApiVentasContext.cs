﻿using ApiVentas.Entities.POCOEntities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Repositories.EFCore.DataContext
{
    public class ApiVentasContext : DbContext
    {
        public ApiVentasContext(DbContextOptions<ApiVentasContext> options) : base(options)
        {

        }

        public DbSet<Cliente> Clientes { get; set; }
        public DbSet<Producto> Productos { get; set; }
        public DbSet<Asesor> Asesores { get; set; }
        public DbSet<Venta> Ventas { get; set; }
        public DbSet<VentaDetalle> VentaDetalles { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<Cliente>()
                .Property(c => c.Nombre)
                .IsRequired()
                .HasMaxLength(40);

            modelBuilder.Entity<Producto>()
                .Property(c => c.Nombre)
                .IsRequired()
                .HasMaxLength(40);

            modelBuilder.Entity<Asesor>()
                .Property(c => c.Nombre)
                .IsRequired()
                .HasMaxLength(40);

            modelBuilder.Entity<Venta>()
                .Property(o => o.ClienteId)
                .IsRequired();

            modelBuilder.Entity<Venta>()
                .HasOne<Cliente>()
                .WithMany()
                .HasForeignKey(o => o.ClienteId);

            modelBuilder.Entity<Venta>()
                .HasOne<Asesor>()
                .WithMany()
                .HasForeignKey(o => o.AsesorId);


            modelBuilder.Entity<VentaDetalle>()
                .HasKey(od => new { od.VentaId, od.ProductoId });

            modelBuilder.Entity<VentaDetalle>()
                .HasOne<Producto>()
                .WithMany()
                .HasForeignKey(od => od.ProductoId);

            
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.CreateVenta
{
    public interface ICreateVentaOutPutPort
    {
        Task Handle(int ventaId);
    }
}

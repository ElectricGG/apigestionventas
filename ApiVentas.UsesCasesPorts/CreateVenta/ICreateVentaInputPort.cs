﻿using ApiVentas.UsesCasesDTOs.CreateVenta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.CreateVenta
{
    public interface ICreateVentaInputPort
    {
        Task Handle(CreateVentaParams ventaParams);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.GetAsesores
{
    public interface IGetAsesoresInputPort
    {
        Task Handle();
    }
}

﻿using ApiVentas.UsesCasesDTOs.GetAsesores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.GetAsesores
{
    public interface IGetAsesoresOutputPort
    {
        Task Handle(IEnumerable<GetAsesoresDTO> getAsesores);
    }
}

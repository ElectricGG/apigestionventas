﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.DeleteVenta
{
    public interface IDeleteVentaInputPort
    {
        Task Handle(int id);
    }
}

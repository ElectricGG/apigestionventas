﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.GetClientes
{
    public interface IGetClientesInputPort
    {
        Task Handle();
    }
}

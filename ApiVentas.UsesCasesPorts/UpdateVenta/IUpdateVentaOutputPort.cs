﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCasesPorts.UpdateVenta
{
    public interface IUpdateVentaOutputPort
    {
        Task Handle(int Id);
    }
}

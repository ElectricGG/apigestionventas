﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.UsesCasesDTOs.GetProductos;
using ApiVentas.UsesCasesPorts.GetProductos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.GetProductos
{
    public class GetProductosInteractor : IGetProductosInputPort
    {
        readonly IProductoRepository _productoRepository;
        readonly IGetProductosOutputPort _outputPort;
        public GetProductosInteractor(IProductoRepository productoRepository,IGetProductosOutputPort outputPort)
            => (_productoRepository, _outputPort) = (productoRepository,outputPort);
        public Task Handle()
        {
            var productos = _productoRepository.GetAll().Select(p=>
            new GetProductosDTO
            {
                Id = p.Id,
                Nombre = p.Nombre,
            });
            _outputPort.Handle(productos);
            return Task.CompletedTask;
        }
    }
}

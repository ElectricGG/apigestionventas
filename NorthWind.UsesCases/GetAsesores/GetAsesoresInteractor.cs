﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.UsesCasesDTOs.GetAsesores;
using ApiVentas.UsesCasesPorts.GetAsesores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.GetAsesores
{
    public class GetAsesoresInteractor : IGetAsesoresInputPort
    {
        readonly IAsesorRepository _asesorRepository;
        readonly IGetAsesoresOutputPort _outputPort;
        public GetAsesoresInteractor(IAsesorRepository asesorRepository, IGetAsesoresOutputPort outputPort)
        => (_asesorRepository, _outputPort) = (asesorRepository, outputPort);
        public Task Handle()
        {
            var asesores = _asesorRepository.GetAll().Select(a=>
            new GetAsesoresDTO
            {
                Id = a.Id,
                Nombre = a.Nombre,
            });
            _outputPort.Handle(asesores);
            return Task.CompletedTask;
        }
    }
}

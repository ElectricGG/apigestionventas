﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.UsesCasesDTOs.GetVentas;
using ApiVentas.UsesCasesPorts.GetVentas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.GetVentas
{
    public class GetVentasInteractor : IGetVentasInputPort
    {
        readonly IGetVentasOutPutPort _getVentaOutputPort;
        readonly IVentaRepository _ventaRepository;
        readonly IVentaDetalleRepository _ventaDetalleRepository;
        readonly IProductoRepository _productoRepository;
        readonly IAsesorRepository _asesorRepository;
        readonly IClienteRepository _clienteRepository;
        
        public GetVentasInteractor(IVentaRepository ventaRepository, IVentaDetalleRepository ventaDetalleRepository, IGetVentasOutPutPort getVentasOutPutPort,
                                   IProductoRepository productoRepository, IAsesorRepository asesorRepository, IClienteRepository clienteRepository)
            => (_ventaRepository, _ventaDetalleRepository, _getVentaOutputPort, _productoRepository, _asesorRepository, _clienteRepository) 
            = (ventaRepository, ventaDetalleRepository, getVentasOutPutPort, productoRepository, asesorRepository, clienteRepository);

        public Task Handle()
        {
            var ventas = _ventaRepository.GetAll().Select(v=>
                new GetVentasDTO
                {
                    Id = v.Id,
                    AsesorId = v.AsesorId,
                    ClienteId = v.ClienteId,
                    FechaVenta = v.FechaVenta
                }).ToList();

            List<GetVentasDetalleDTO> ventaDetalles = _ventaDetalleRepository.GetAll().Select(vd =>
            new GetVentasDetalleDTO
            {
                VentaId = vd.VentaId,
                ProductoId = vd.ProductoId,
                PrecioUnitario = vd.PrecioUnitario,
                Cantidad = vd.Cantidad,
                Subtotal = vd.Cantidad * vd.PrecioUnitario
            }).ToList();

            ventas.ForEach(v =>
                v.ventaDetalles = ventaDetalles.Where(vd => vd.VentaId == v.Id)
            );

            var asesores = _asesorRepository.GetAll().Select(a => new AsesorGetVentaDTO { Id=a.Id, Nombre = a.Nombre}).ToList();
            var clientes = _clienteRepository.GetAll().Select(c => new ClienteGetVentaDTO { Id = c.Id, Nombre = c.Nombre }).ToList();
            var productos = _productoRepository.GetAll().Select(p => new ProductoGetVentasDetalleDTO { Id = p.Id, Nombre = p.Nombre  }).ToList();

            ventas.ForEach(v =>
            {
                v.asesor = asesores.Where(a => a.Id == v.AsesorId).FirstOrDefault();
            });

            ventas.ForEach(v =>
            {
                v.cliente = clientes.Where(c => c.Id == v.ClienteId).FirstOrDefault();
            });

            ventaDetalles.ForEach(vd =>
                vd.producto = productos.Where(p => p.Id == vd.ProductoId).FirstOrDefault()
            );

            ventas.ForEach(v => 
            v.MontoVenta = ventaDetalles.Where(d => d.VentaId==v.Id).Sum(x=> x.Subtotal)
            );

            _getVentaOutputPort.Handle(ventas);
            return Task.CompletedTask;
        }
    }
}

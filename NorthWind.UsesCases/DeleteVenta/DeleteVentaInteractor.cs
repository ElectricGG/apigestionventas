﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.UsesCasesPorts.DeleteVenta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.DeleteVenta
{
    public class DeleteVentaInteractor : IDeleteVentaInputPort
    {
        readonly IVentaRepository _ventaRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly IDeleteVentaOutPutPort _outPutPort;

        public DeleteVentaInteractor(IVentaRepository ventaRepository, IUnitOfWork unitOfWork, IDeleteVentaOutPutPort outPutPort)
            => (_ventaRepository, _unitOfWork, _outPutPort) = (ventaRepository, unitOfWork, outPutPort);

        public async Task Handle(int id)
        {
            var ventaToDlt = _ventaRepository.GetVentaById(id);
            _ventaRepository.Delete(ventaToDlt);
            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            await _outPutPort.Handle(id);
        }
    }
}

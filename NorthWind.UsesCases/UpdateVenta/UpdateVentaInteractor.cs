﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.UsesCasesDTOs.UpdateVenta;
using ApiVentas.UsesCasesPorts.UpdateVenta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.UpdateVenta
{
    public class UpdateVentaInteractor : IUpdateVentaInputPort
    {
        readonly IVentaRepository _ventaRepository;
        readonly IVentaDetalleRepository _detalleRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly IUpdateVentaOutputPort _updateVentaOutputPort;

        public UpdateVentaInteractor(IVentaRepository ventaRepository,
                                    IVentaDetalleRepository ventaDetalleRepository,
                                    IUnitOfWork unitOfWork,
                                    IUpdateVentaOutputPort updateVentaOutputPort)
            => (_ventaRepository, _detalleRepository, _unitOfWork, _updateVentaOutputPort) = (ventaRepository,ventaDetalleRepository,unitOfWork,updateVentaOutputPort);
        
        public async Task Handle(UpdateVentaParams ventaParams)
        {
            Venta venta = new Venta
            {
                Id = ventaParams.Id,
                AsesorId = ventaParams.AsesorId,
                ClienteId = ventaParams.ClienteId,
                FechaVenta = ventaParams.FechaVenta
            };

            _ventaRepository.Update(venta);

            foreach (var Item in ventaParams.VentaDetalles)
            {
                _detalleRepository.Update(new VentaDetalle
                {
                    Venta = venta,
                    ProductoId = Item.ProductoId,
                    PrecioUnitario = Item.PrecioUnitario,
                    Cantidad = Item.Cantidad,
                    VentaId = venta.Id
                });
            }

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            await _updateVentaOutputPort.Handle(venta.Id);
        }
    }
}

﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.Entities.POCOEntities;
using ApiVentas.UsesCasesDTOs.CreateVenta;
using ApiVentas.UsesCasesPorts.CreateVenta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.CreateVenta
{
    public class CreateVentaInteractor : ICreateVentaInputPort
    {
        readonly IVentaRepository _ventaRepository;
        readonly IVentaDetalleRepository _detalleRepository;
        readonly IUnitOfWork _unitOfWork;
        readonly ICreateVentaOutPutPort _outPutPort;

        public CreateVentaInteractor( IVentaRepository ventaRepository, IVentaDetalleRepository ventaDetalleRepository, ICreateVentaOutPutPort outPutPort, IUnitOfWork unitOfWork)
            => (_ventaRepository, _detalleRepository, _unitOfWork, _outPutPort) = (ventaRepository, ventaDetalleRepository, unitOfWork, outPutPort);

        public async Task Handle(CreateVentaParams ventaParams)
        {
            Venta venta = new Venta {
                AsesorId = ventaParams.AsesorId,
                ClienteId = ventaParams.ClienteId,
                FechaVenta = ventaParams.FechaVenta
            };

            _ventaRepository.Create(venta);

            foreach(var Item in ventaParams.VentaDetalles)
            {
                _detalleRepository.Create(new VentaDetalle
                {
                    Venta = venta,
                    ProductoId = Item.ProductoId,
                    PrecioUnitario = Item.PrecioUnitario,
                    Cantidad = Item.Cantidad
                });
            }

            try
            {
                await _unitOfWork.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            await _outPutPort.Handle(venta.Id);
        }
    }
}

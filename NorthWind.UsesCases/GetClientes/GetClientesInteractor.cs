﻿using ApiVentas.Entities.Interfaces;
using ApiVentas.UsesCasesDTOs.GetClientes;
using ApiVentas.UsesCasesPorts.GetClientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.UsesCases.GetClientes
{
    public class GetClientesInteractor: IGetClientesInputPort
    {
        readonly IClienteRepository _clienteRepository;
        readonly IGetClientesOutputPort _outputPort;
        public GetClientesInteractor(IClienteRepository clienteRepository, IGetClientesOutputPort outputPort)
        => (_clienteRepository, _outputPort) = (clienteRepository, outputPort);
        public Task Handle()
        {
            var clientes = _clienteRepository.GetAll().Select(c=>
            new GetClientesDTO
            {
                Id = c.Id,
                Nombre = c.Nombre
            });
            _outputPort.Handle(clientes);
            return Task.CompletedTask;
        }
    }
}

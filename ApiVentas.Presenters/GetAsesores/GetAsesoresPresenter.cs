﻿using ApiVentas.UsesCasesDTOs.GetAsesores;
using ApiVentas.UsesCasesPorts.GetAsesores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.GetAsesores
{
    public class GetAsesoresPresenter : IGetAsesoresOutputPort, IPresenter<IEnumerable<GetAsesoresDTO>>
    {
        public IEnumerable<GetAsesoresDTO> Content { get; private set; }

        public Task Handle(IEnumerable<GetAsesoresDTO> getAsesores)
        {
            Content = getAsesores;
            return Task.CompletedTask;
        }
    }
}

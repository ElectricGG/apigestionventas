﻿using ApiVentas.UsesCasesDTOs.GetClientes;
using ApiVentas.UsesCasesPorts.GetClientes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.GetClientes
{
    public class GetClientesPresenter : IGetClientesOutputPort, IPresenter<IEnumerable<GetClientesDTO>>
    {
        public IEnumerable<GetClientesDTO> Content { get; private set; }

        public Task Handle(IEnumerable<GetClientesDTO> getClientes)
        {
            Content = getClientes;
            return Task.CompletedTask;
        }
    }
}

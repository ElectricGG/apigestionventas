﻿using ApiVentas.UsesCasesDTOs.GetProductos;
using ApiVentas.UsesCasesPorts.GetProductos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.GetProductos
{
    public class GetProductosPresenter : IGetProductosOutputPort, IPresenter<IEnumerable<GetProductosDTO>>
    {
        public IEnumerable<GetProductosDTO> Content { get; private set; }

        public Task Handle(IEnumerable<GetProductosDTO> getProductos)
        {
            Content = getProductos;
            return Task.CompletedTask;
        }
    }
}

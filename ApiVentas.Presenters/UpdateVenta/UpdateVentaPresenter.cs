﻿using ApiVentas.UsesCasesPorts.UpdateVenta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.UpdateVenta
{
    public class UpdateVentaPresenter : IUpdateVentaOutputPort, IPresenter<string>
    {
        public string Content { get; private set; }

        public Task Handle(int Id)
        {
            Content = $"Venta ID: {Id} se actualizó";
            return Task.CompletedTask;
        }
    }
}

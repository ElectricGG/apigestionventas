﻿using ApiVentas.UsesCasesPorts.DeleteVenta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.DeleteVentas
{
    public class DeleteVentaPresenter : IDeleteVentaOutPutPort, IPresenter<string>
    {
        public string Content { get; private set; }

        public Task Handle(int id)
        {
            Content = $"La venta con ID: {id} ha sido eliminada correctamente.";
            return Task.CompletedTask;
        }
    }
}

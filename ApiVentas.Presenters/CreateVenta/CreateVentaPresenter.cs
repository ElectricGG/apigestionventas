﻿using ApiVentas.UsesCasesPorts.CreateVenta;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.VentaPresenter
{
    public class CreateVentaPresenter : ICreateVentaOutPutPort, IPresenter<string>
    {
        public string Content {get; private set;}

        public Task Handle(int ventaId)
        {
            Content = JsonConvert.SerializeObject($"Venta ID: {ventaId}");
            return Task.CompletedTask;
        }
    }
}

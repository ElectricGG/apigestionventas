﻿using ApiVentas.UsesCasesDTOs.GetVentas;
using ApiVentas.UsesCasesPorts.GetVentas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApiVentas.Presenters.GetVentas
{
    public class GetVentasPresenter : IGetVentasOutPutPort,IPresenter<IEnumerable<GetVentasDTO>>
    {
        public IEnumerable<GetVentasDTO> Content { get; private set; }
        public Task Handle(IEnumerable<GetVentasDTO> orders)
        {
            Content = orders;
            return Task.CompletedTask;
        }
    }
}
